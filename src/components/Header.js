import React from 'react';

function Header(props) {
    const {handleType} = props;
    const {handleStatus} = props;
    const {type} = props;
    const {status} = props;

    return (
        <>
            <div className="bg-light-red fl w-100">
                <header className="tcx pl3 pr3 pb3 pt4">
                    <h2 className="f2 gray fw6 white ma0 nowrap mw8 center">Schedule</h2>
                </header>

                <div className="tc nowrap mw8 center">
                    <div className={'pointer f6 fl w-third pa3 ' + (type === 'All' ? 'white bb': 'white-50') }
                         onClick={()=>handleType('All')}>
                        All
                    </div>
                    <div className={'pointer f6 fl w-third pa3 ' + (type === 'international' ? 'white bb': 'white-50') }
                         onClick={()=>handleType('international')}>
                        International
                    </div>
                    <div className={'pointer f6 fl w-third pa3 ' + (type === 'domestic' ? 'white bb': 'white-50') }
                         onClick={()=>handleType('domestic')}>
                        Domestic
                    </div>
                </div>
            </div>

            <div className="tc fl w-100 bb b--black-10 bg-white">
                <div className="nowrap mw8 center">
                    <div className={'pointer f6 fl w-third pa3 gray ' + (status === 'upcoming' ? 'light-red bb': 'white-50') }
                         onClick={()=>handleStatus('upcoming')}>
                        Upcoming
                    </div>
                    <div className={'pointer f6 fl w-third pa3 gray ' + (status === 'running' ? 'light-red bb': 'white-50') }
                         onClick={()=>handleStatus('running')}>
                        Running
                    </div>
                    <div className={'pointer f6 fl w-third pa3 gray ' + (status === 'completed' ? 'light-red bb ': 'white-50') }
                         onClick={()=>handleStatus('completed')}>
                        Completed
                    </div>
                </div>
            </div>
        </>
    );
}
export default Header;
