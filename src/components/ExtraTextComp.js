import React from 'react';

function ExtraTextComp(props) {
    const {string} = props;
    return (
        <div className="fl w-100">
            <p className="tc pa5 dark-grey f5 fw3 ttu tracked light-red">{string}</p>
        </div>
    );
}
export default ExtraTextComp;
