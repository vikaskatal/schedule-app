import React from 'react';
import moment from 'moment';

function MatchItem(props) {
    const {matchInfo} = props;

    return (
        <div className="fl w-100 w-50-l pa2">
            <article className=" center bg-white pa3 ba b--black-10 br2">
                <div className="">
                    <h1 className="f5 lh-copy">{matchInfo.seriesName}</h1>
                </div>
                <p className=" measure f4 black-70 mt1"> {`${matchInfo.matchType}, ${matchInfo.venue}`}</p>
                <p className="lh-copy f6 black-70 mv1">Home Team: {matchInfo.homeTeamName}</p>
                <p className="lh-copy f6 black-70 mv1">Other Team: {matchInfo.awayTeamName}</p>
                {
                    matchInfo.matchResult ? <p className="lh-copy f7 mt4 pt2 mb1 light-red bt b--light-gray">Status: {matchInfo.matchResult}</p> : null
                }
                {
                    matchInfo.toss &&  matchInfo.matchStatus === 'live'? <p className="lh-copy f7 mt4 pt2 mb1 light-red bt b--light-gray">Toss: {matchInfo.toss}</p> : null
                }
                {
                    matchInfo.startDate &&  matchInfo.matchStatus === 'upcoming'? <p className="lh-copy f7 mt4 pt2 mb1 light-red bt b--light-gray">Match starts { moment.unix(matchInfo.startDate ).startOf('day').fromNow() } </p> : null
                }
            </article>
        </div>

    );
}
export default MatchItem;
