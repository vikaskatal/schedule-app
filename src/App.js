import React, { useState, useEffect } from 'react';
import Header from './components/Header';
import MatchItem from './components/MatchItem';
import ExtraTextComp from './components/ExtraTextComp';
import 'tachyons/css/tachyons.min.css';


function App() {
    const [loading, setLoading] = useState(true);
    const [match, setMatch] = useState([]);
    const [type, setType] = useState("All");
    const [status, setStatus] = useState("upcoming");
    const [page, setPage] = useState(0);

    useEffect(() => {
        console.log('useEffect called');
        const query = `
              query getMatchSchedule {
                    schedule(type: "${type}", status: "${status}", page: ${page}) {
                        seriesID
                        seriesName
                        homeTeamName
                        awayTeamName
                        toss
                        matchStatus
                        matchType
                        venue
                        matchResult
                        startDate
                    }
                }
            `;
        const url = "https://api.devcdc.com/cricket";
        const opts = {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({ query })
        };
        fetch(url, opts)
            .then(res => res.json())
            .then(response =>{
                setLoading(false);
                setMatch(response.data.schedule)
            })
            .catch(()=>{
                setLoading(false);
            });
    }, [type, status, page]);

    const handleType = (typeVal) => {
        if(type !== typeVal){
            setLoading(true);
            setType(typeVal);
        }
    };

    const handleStatus = (status) => {
        setLoading(true);
        setStatus(status);
    };

    const handlePage = (arg) => {
        setLoading(true);
        let currentPage = page;
        if(arg === 'next'){
            currentPage = currentPage + 1
        }
        if(arg === 'prev'){
            if(currentPage !== 0){
                currentPage = currentPage - 1
            }
        }
        setPage(currentPage)
    };

    return (
        <div className="bg-near-white min-vh-100">
            <Header type={type} status={status} handleStatus={handleStatus} handleType={handleType} />

            {
                loading ?
                    <ExtraTextComp string={'Loading...'}/> :  match.length ?
                        <div className="fl w-100 pa2 ma0">
                            <div className="nowrap mw8 center"> {
                                match.map((ele) => <MatchItem key={ele.matchID} matchInfo={ele} />)
                            }
                            </div>
                        </div> :  <ExtraTextComp string={'No data found'}/>
            }

            <div className="flex items-center justify-center pa3">
                <div onClick={()=> page > 0 ? handlePage('prev') : null}
                     className="pointer f5 no-underline light-red bg-animate hover-bg-light-red hover-white inline-flex items-center pv2 ph3 ba border-box mr4">
                    <span>Prev</span>
                </div>
                <div onClick={()=>handlePage('next')}
                     className="pointer f5 no-underline light-red bg-animate hover-bg-light-red hover-white inline-flex items-center pv2 ph3 ba border-box">
                    <span>Next</span>
                </div>
            </div>


        </div>
    );
}

export default App;
